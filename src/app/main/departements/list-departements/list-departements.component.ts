import { Component, OnInit } from '@angular/core';
import { DepartementsService } from 'src/app/data/services/departements/departements.service';
import { Departement } from 'src/app/data/models/departement.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-departements',
  templateUrl: './list-departements.component.html',
  styleUrls: ['./list-departements.component.css']
})
export class ListDepartementsComponent implements OnInit {
  public chargementEnCours: boolean = true;
  public departements: Departement[] = [];
  public formDepartements: FormGroup;
  get departement() { return this.formDepartements.get('departement') }

  constructor(
    private readonly departementsService: DepartementsService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.loadData().then((result) => {
      if (result) {
        // Instancie le formulaire de sélection
        this.formDepartements = this.fb.group({
          departement: [undefined, Validators.required]
        });

        this.chargementEnCours = false;
      } else {
        alert('Erreur lors du chargement des départements !');
      }
    });
  }

  /** 
   * Charge la liste des départements 
   * @returns Une promesse à vraie si chargement réussi, faux si non
   */
  private loadData(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.departementsService.getDepartements().subscribe(result => {
        this.departements = result;
        resolve(true);
      }, error => reject(false));
    });
  }

  /** Redirige vers le détail du département sélectionné */
  public onSubmit(): void {
    if (this.formDepartements.valid) {
      this.router.navigate(['/departement/', this.departement.value.nom]);
    } else {
      alert('Veuillez sélectionner un département.');
    }
  }
}
