import { Component, OnInit } from '@angular/core';
import { Departement } from 'src/app/data/models/departement.model';
import { DepartementsService } from 'src/app/data/services/departements/departements.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-departement',
  templateUrl: './detail-departement.component.html',
  styleUrls: ['./detail-departement.component.css']
})
export class DetailDepartementComponent implements OnInit {
  public chargementEnCours: boolean = true;
  public departement: Departement;

  constructor(
    private readonly departementsService: DepartementsService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit() {
    // Charger le département depuis le nom passé dans l'URL
    this.route.paramMap.subscribe(params => {
      this.departementsService.getDepartement(params.get('nom')).subscribe(result => {
        this.departement = result;
        this.chargementEnCours = false;
      });
    });
  }

  public onReturn(): void {
    this.router.navigate(['']);
  }
}
