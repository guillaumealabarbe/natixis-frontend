import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Departement } from '../../models/departement.model';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DepartementsService {
    constructor(private readonly http: HttpClient) { }

    public getDepartements(): Observable<Departement[]> {
        return this.http.get<Departement[]>('http://localhost:8080/departements');
    }

    public getDepartement(nom: string): Observable<Departement> {
        return this.http.get<Departement[]>(`http://localhost:8080/departements?nom=${nom}`).pipe(
            map(result => result[0])
        );
    }
}