import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ListDepartementsComponent } from './main/departements/list-departements/list-departements.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailDepartementComponent } from './main/departements/detail-departement/detail-departement.component';
import { RouterModule, Routes } from '@angular/router';

// Externaliser les routes dans un fichier externe
const routes: Routes = [
  { path: '', component: ListDepartementsComponent },
  { path: 'departement/:nom', component: DetailDepartementComponent },
  { path: '**', component: ListDepartementsComponent } // Ajouter un composant NotFoundComponent
]

@NgModule({
  declarations: [
    AppComponent,
    ListDepartementsComponent,
    DetailDepartementComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
